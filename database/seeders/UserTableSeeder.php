<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = '';
        $password = '';

        while (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = $this->command->ask('Admin login email', 'info@example.com');
        }

        while (strlen($password) < 8) {
            $password = $this->command->ask('Admin login password (min 8 characters)');
        }

        $user = User::create([
            'name' => 'admin',
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        Role::firstOrCreate([
            'name' => 'admin'
        ]);

        $user->assignRole('admin');
    }
}